# 1048576/image.d/nginx

```dockerfile
ARG BUILD_ARG_APP_GROUP_ID=65534
ARG BUILD_ARG_APP_USER_ID=65534

ARG BUILD_ARG_NGINX_IMAGE_REGISTRY=""
ARG BUILD_ARG_NGINX_IMAGE_TAG=""
ARG BUILD_ARG_NGINX_IMAGE_NAME="server-debug"

FROM ${BUILD_ARG_NGINX_IMAGE_REGISTRY}/${BUILD_ARG_NGINX_IMAGE_NAME}:${BUILD_ARG_NGINX_IMAGE_TAG} as web-site-frontend
ARG BUILD_ARG_APP_GROUP_ID
ARG BUILD_ARG_APP_USER_ID
...
RUN /build.d/bin/pre-build.sh
...
RUN /build.d/bin/post-build.sh
USER www-data:www-data
```

```yaml
services:
  web-site-frontend:
    build:
      args:
        BUILD_ARG_APP_GROUP_ID: "${BUILD_ARG_APP_GROUP_ID}"
        BUILD_ARG_APP_USER_ID: "${BUILD_ARG_APP_USER_ID}"
      context: "./"
      dockerfile: "./images/client/site/Dockerfile"
      target: "web-site-frontend"
    environment:
      TZ: "${TZ}"
    networks:
      local: {}
    ports:
      - "${WEB_SITE_FRONTEND_PORT}:8080"
```

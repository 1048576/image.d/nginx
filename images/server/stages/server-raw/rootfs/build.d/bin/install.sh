#!/usr/bin/env sh

set -o errexit
set -o nounset
set -o pipefail

function _sh {
    local command=$1
    echo -e "\e[1;34m$(pwd)\e[32m\$ ${command}\e[39m"
    sh -c "${command}"
}

# Install nginx
_sh "apk add --no-cache nginx~${BUILD_ARG_NGINX_VERSION}"

# Remove users
_sh "deluser nobody"

# Remove groups
_sh "delgroup nogroup"

# Install shadow
_sh "apk add --no-cache shadow"

# Install bash
_sh "apk add --no-cache bash"

# Remove default folders and files
_sh "rm -rf /etc/nginx/*"
_sh "rm -rf /var/www/"

# Log
_sh "ln -sf /dev/stdout /var/lib/nginx/logs/access.log"
_sh "ln -sf /dev/stdout /var/lib/nginx/logs/error.log"

_sh "nginx -v"

# Post install
_sh "rm $0"

#!/usr/bin/env sh

set -o errexit
set -o nounset
set -o pipefail

function _cd {
    local path=$1
    echo -e "\e[1;34m$(pwd)\e[32m\$ cd ${path}\e[39m"
    cd ${path}
}

function _sh {
    local command=$1
    echo -e "\e[1;34m$(pwd)\e[32m\$ ${command}\e[39m"
    sh -c "${command}"
}

spnego_http_auth_module_version="1.1.1"

# Install build tools
_sh "apk add --virtual .build-deps g++ make"
_sh "mkdir ./target/"

# nginx
_sh "wget http://hg.nginx.org/nginx/archive/release-${BUILD_ARG_NGINX_VERSION}.tar.gz -O nginx.tar.gz"
_sh "tar xf nginx.tar.gz -C ./target/"

# spnego-http-auth-nginx-module
_sh "wget https://codeload.github.com/stnoonan/spnego-http-auth-nginx-module/tar.gz/refs/tags/v${spnego_http_auth_module_version} -O spnego-http-auth-nginx-module.tar.gz"
_sh "tar xf spnego-http-auth-nginx-module.tar.gz -C ./target/"
_sh "apk add --virtual .build-deps-spnego-http-auth-nginx-module krb5-dev"

# Build
_cd "./target/nginx-release-${BUILD_ARG_NGINX_VERSION}/"
_sh "./auto/configure --without-http_rewrite_module --without-http_gzip_module --add-dynamic-module=../spnego-http-auth-nginx-module-${spnego_http_auth_module_version}/"
_sh "make"
_sh "mkdir -p /etc/nginx/modules/"
_sh "find ./objs -name '*.so' | xargs -I {} cp {} /etc/nginx/modules/"

_sh "apk del .build-deps"
_sh "apk del .build-deps-spnego-http-auth-nginx-module"
